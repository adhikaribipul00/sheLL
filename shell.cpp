#include "shell.h"


void read_config(){
    //File to read the configuration from
    std::ifstream config;
    std::string line;
    config.open("/home/bipgen/shell_config");
    //Fail first if not found config
    if (!config.is_open()){
        std::cout<<"Could not file config. file. Abort.";
        abort();
    }
    while(true){
        if (config.eof() ) break;
        config >> line;
        process_line(line);
    }

}

pid_t child;

int main(){
    int wstatus;
    //For IPC
    int coms[2];
    char buf[4096];

    //Initializing the environment
    
    init_environ();


    if ( pipe(coms) == -1 ) die("Pipe is screwed");
    read_config();
    while (true){
        memset(buf,NULL,sizeof(buf));
        show_shell();
        std::string path = exec_cmd();
        if (path.empty()) continue;
        char *cstr = new char[64];
        strcpy(cstr, options.c_str());
        if (options.empty()) cstr = NULL;
        options.clear();
        char *opts = {NULL};
        child = fork();

        if (child != 0){
            close(coms[1]);
            wait(NULL);
            int nbytes = read(coms[0],buf,sizeof(buf));
            std::cout<<buf;
        }

        //Child process
        if (child == 0){
            dup2(coms[1],STDOUT_FILENO);
            close(coms[0]);
            close(coms[1]);
            char *str = new char[64];
            strcpy(str,path.c_str());
            char *argv[] =  {str,cstr,NULL};
            execv(path.c_str(),argv);
        }
    }
}

