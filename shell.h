#ifndef __DSHELL__
#define __DSHELL__

#include<iostream>
#include<cstdlib>
#include<unistd.h>
#include<dirent.h>
#include<cstring>
#include<glob.h>
#include<algorithm>
#include<vector>
#include <boost/algorithm/string.hpp>
#include<fstream>
#include<sys/types.h>
#include<sys/wait.h>
#define BOOST_NO_CXX11_SCOPED_ENUMS
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include<map>

#undef BOOST_NO_CXX11_SCOPED_ENUMS

#define die(e) do { fprintf(stderr, "%s\n", e); exit(EXIT_FAILURE); } while (0);

namespace fs = boost::filesystem;
//Lets work on the shell amigos

//So what do we have to do?
//We need a path variable to look for the commands
//We need to fork into those commands execute them with exc
//Once they execute we need to store the output in some variable
//
//How to do we define a path? 
//We create a configuration file for our shell
//
//What do we do into path directories?
//Look for filenames in those paths
//?? Open those directories and use glob to match patterns 
// 

//First?? Globals what do we need
std::vector<std::string> paths;
std::string cmd;
std::string input;
std::string options;	//Might end up not using this
std::string shell_sym = "$";

extern char **environ;
//Additional ds hashmap for the environment variables
std::map<std::string,std::string> vars;

//Setting up prototoypes
void cd(const char*);
std::string get_var(std::string);
std::string get_value(std::string);
void add_to_map(std::string,std::string);

//Get the input
void get_input(){
	char ip[1000];
	std::cin.getline(ip,sizeof(ip));
	input = ip;
	//Find and replace the variables with its value
	std::string var = get_var(input);
	//If there is a variable
	if( !var.empty() ){
		//We have a variable we have to replace it with the value of it
		std::size_t start = input.find("$");
		std::size_t end = input.find(" ",start);
		if (input.find("/",start) < end) end = input.find("/",start);
		std::string value = get_value(var);
		if (!value.empty()) input.replace(start,end-start,value);
		//If no such variable is there then replace it with blank
		else	input.replace(start,end-start,"");
	}

	//Find the cmd
	if (input.find(' ') != -1){
		cmd = input.substr(0,input.find(' '));
		options = input.substr(input.find_first_of(' ') + 1,input.length() - input.find_first_of(' '));
	}
	else{
		cmd = input;
	}
}

void print(std::string out){
	std::cout<<out<<std::endl;
}

void set_symbol(std::string symbol){
	shell_sym = symbol;
}


void read_paths(std::string input){
	//Get the various paths to be searched in
	boost::split(paths,input,boost::is_any_of(":"));
}

void process_line(std::string input){
	//Config option type
	std::string opt = input.substr(0,input.find_first_of('='));
	std::transform(opt.begin(),opt.end(),opt.begin(),::toupper);
	if (opt == "PATH" ){
		read_paths(input.substr(input.find('=')+ 1,input.length() - input.find_first_of('=')));
	}
	else if (opt == "SYMBOL" ){
		set_symbol(input.substr(input.find('=')+ 1,input.length() - input.find_first_of('=')));
	}
}
/**
Returns 0 is the command issued is inbuilt to the shell like cd.
Returns -1 if the command issues is to searched from the paths. 
*/
int process_options(){
	//This will check if the command sent is one of the command that are inbuilt
	if (cmd == "cd"){
        char *cstr = new char[64];
        strcpy(cstr, options.c_str());
        if (options.empty()) cstr = NULL;
		options.clear();
		cd(cstr);
		return 0;
	}
	else if (cmd == "quit"){
		exit(0);
	}
	else{
		return -1;
	}
}

std::string find_path(){
	//Find path of the command 
	//sd is our search directory
	std::vector<std::string> loc;
	glob_t pglob;
	for (std::vector<std::string>::iterator it = paths.begin(); it!=paths.end(); it++){
		std::string str = * it;
		str.append("/");
		str.append(cmd);
		//Changing it to path
		fs::path mpath(str);
		if ( fs::exists(mpath) ){
			return mpath.string();
		}
	}
	return "";
}


void show_shell(){
	char pwd[128];
	getcwd(pwd,sizeof(pwd));	

	std::cout<<pwd<<shell_sym;
	get_input();
	//Once we know the path of the command lets execute it
}

std::string exec_cmd(){
	std::string path = find_path();
	if (path.empty()) std::cout<<" not found"<<std::endl;
	return path;
}

/**
This function changes the directory 
*/
void cd(const char* path){
	if ( chdir(path) == -1){
		perror(strerror(errno));
	}
}

/**
 * Adding support for variables
 */
std::string get_var(std::string text){
	//Check if the string contains a $ sign
	std::size_t found = text.find("$");
	
	if(found == std::string::npos){
		return {};
	}
	
	//In case its found then lets get going with the coding
	//Now lets find the whitespace after the $ sign
	std::size_t var_len = text.find(" ",found);
	//It might be the end of the line so lets add that possibility
	if(var_len == std::string::npos){
		var_len = text.find("/",found);
		
	}
	if(var_len == std::string::npos){
		var_len = text.size();
		
	}
	
	//If the variable is present then lets extract its name and return name
	return text.substr(found + 1,var_len - found - 1);
}

std::string get_value(std::string variable){
	std::map<std::string,std::string>::iterator it = vars.find(variable);
	if (it == vars.end()) return {};
	else return it->second;
}


//We need some data structure hashmap to hold all the variables

void init_environ(){
	std::size_t len = 0;
	while (environ[len] != NULL) len++;
	for (int  i = 0; i < len; i++){
		//Lets divide the first var into var_name and var_value
		//We need to find the first position of equals
		std::string env_temp(environ[i]);
		std::size_t delim = env_temp.find("=");
		//Error 
		if(delim == std::string::npos) exit -1;
		
		//Var name
		std::string var_name = env_temp.substr(0,delim);
		std::string value = env_temp.substr(delim+1);
		add_to_map(var_name,value);
	}
	
}

//Adding to the map
void add_to_map(std::string var_name,std::string value){
	vars[var_name] = value;
	return;
}

/**Method for debugging the map 
 * */

void iterate_map(){
	for( std::map<std::string,std::string>::iterator it = vars.begin(); it != vars.end(); ++it){
		std::cout<<"Var name = "<<it->first<<" Value = "<<it->second<<std::endl;
	}
}


#endif



